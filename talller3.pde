import ddf.minim.*;
import javax.swing.JOptionPane;

Minim minim;
AudioPlayer player;


int y1=399;
int vy1;
int vy2;
int y2=399;
int n1=1;
int n2=1;
void setup(){
size(600,600);
background(0);   
noFill();

minim = new Minim(this);
player = minim.loadFile("wav_alarma.mp3");



//nombres para los indicadores y sus respectivas medidas
PFont font;
font=loadFont("BellMT-32.vlw");
textFont(font);

fill(255,255,255);
text("Presion",140,450);
text("Temperatura",335,450);

text("0 -",116,405);
text("25 -",100,327);
text("50 -",100,246);
text("75 -",100,165);
text("100 -",85,86);
text("Pa", 85,50);

text("0 -",341,405);
text("25 -",325,327);
text("50 -",325,246);
text("75 -",325,165);
text("100 -",310,86);
text("°C", 310,50);


}

void draw() {
if(y1==75)
{
  player.play();
  n1 = JOptionPane.showConfirmDialog(null, "Presion alta, desea reiniciar el sistema?", "Cuidado!", JOptionPane.YES_NO_OPTION);
 // alarma visual y auditiva 
}
if(y2==75)
{
  player.play();
  n2 = JOptionPane.showConfirmDialog(null, "Temperatura alta, desea reiniciar el sistema?", "Cuidado!", JOptionPane.YES_NO_OPTION);
   // alarma visual y auditiva 
}

  paint();
  //Si el cursor sobre uno de los indicadores se rellenan, cursor fuera se vacian y limites para que no se salgan del medidor  
  if(n1==0){
     y1=400;
     n1=1;
    player.pause();}
  else if(n2==0){
     y2=400;
     n2=1;
   player.pause();}
  else if (abs(mouseX) < 225 &&
      abs(mouseX) > 150 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y1>75
      ){
        vy1=1;
        y1 = y1-vy1;
    }
  else if (abs(mouseX) < 225 &&
      abs(mouseX) > 150 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y1==400
      ){
        vy1=0;
        y1 = y1-vy1;
    }
  else if (abs(mouseX) < 225 &&
      abs(mouseX) > 150 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y1==75
      ){
        vy1=0;

    }
    else if (abs(mouseX) > 225 &&
      y1==400
      ){
        vy1=0;
    }
    else if (abs(mouseX) < 150 &&
      y1==400
      ){
        vy1=0;
    }
    else if (abs(mouseY) < 75 &&
      y1==400
      ){
        vy1=0;
    }
    else if (abs(mouseY) > 400 &&
      y1==400
      ){
        vy1=0;
    }
    

   
  else {
      y1=y1+vy1;}
      
      
        
  if (abs(mouseX) < 450 &&
      abs(mouseX) > 375 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y2>75){
      vy2 = 1;
      y2 = y2-vy2;} 
      
      
else if (abs(mouseX) < 450 &&
      abs(mouseX) > 375 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y2==400
      ){
        vy2=0;
        y2 = y2-vy2;
    }
  else if (abs(mouseX) < 450 &&
      abs(mouseX) > 375 &&
      abs(mouseY) > 75 &&
      abs(mouseY) < 400 &&
      y2==75
      ){
        vy2=0;

    }
    else if (abs(mouseX) > 450 &&
      y2==400
      ){
        vy2=0;
    }
    else if (abs(mouseX) < 375 &&
      y2==400
      ){
        vy2=0;
    }
    else if (abs(mouseY) < 75 &&
      y2==400
      ){
        vy2=0;
    }
    else if (abs(mouseY) > 400 &&
      y2==400
      ){
        vy2=0;
    }      
      
      
      
      
      
      
      
      
  else {
      y2=y2+vy2;}     
}




//indicador visual del estado actual de los medidores
  
void paint(){
stroke(255,255,255);
strokeWeight(1);
fill(255,255,255);
quad(150,75,225,75,225,400,150,400);


stroke(255,255,255);
strokeWeight(1);
fill(255,255,255);
quad(375,75,450,75,450,400,375,400);
  
  
  
  
  
stroke(255,255,255);
strokeWeight(1);
fill(255,0,0);
quad(150,y1,225,y1,225,400,150,400);

stroke(255,255,255);
strokeWeight(1);
fill(255,0,0);
quad(375,y2,450,y2,450,400,375,400);
}
